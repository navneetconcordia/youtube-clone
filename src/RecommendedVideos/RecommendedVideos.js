import React from "react";
import "./RecommendedVideos.css";
import VideoCard from "./VideoCard";
import * as banners from './index';

function RecommendedVideos() {
  return (
    <div className="recommendedvideos">
      <div className="recommendedVideos__videos">
        <VideoCard
          title="Dua Lipa - New Rules (Music Video)"
          views="1.5B"
          image={banners.dua}
          channel="Navneet"
          channelImage= {banners.navneet}
          timestamp="1 year ago"
        />
        <VideoCard
          title="Maroon 5 - Girls Like You ft. Cardi B (Music Video)"
          views="3.4B"
          image={banners.maroon}
          channel="Navneet"
          channelImage={banners.navneet}
          timestamp="2 years ago"
        />
        <VideoCard
          title="Justin Bieber - Intentions ft. Quavo (Music Video)"
          views="800M"
          image={banners.justin}
          channel="Navneet"
          channelImage={banners.navneet}
          timestamp="1 year ago"
        />

        <VideoCard
          title="Taylor Swift - Love Story (Audio)"
          views="300M"
          image={banners.taylor}
          channel="Navneet"
          channelImage={banners.navneet}
          timestamp="7 years ago"
        />

        <VideoCard
          title="Rihanna - Diamonds"
          views="560M"
          image={banners.rihanna}
          channel="Navneet"
          channelImage={banners.navneet}
          timestamp="9 years ago"
        />

        <VideoCard
          title="Ed Sheeran - Shape of You (Music Video)"
          views="5.2B"
          image={banners.sheeran}
          channel="Navneet"
          channelImage={banners.navneet}
          timestamp="3 years ago"
        />

        <VideoCard
          title="Shawn Mendes, Camila Cabello - Señorita"
          views="230M"
          image={banners.senorita}
          channel="Navneet"
          channelImage={banners.navneet}
          timestamp="3 years ago"
        />

        <VideoCard
          title="Bebe Rexha - Meant to Be (feat. Florida Georgia Line) [Official Music Video]"
          views="320M"
          image={banners.rexha}
          channel="Navneet"
          channelImage={banners.navneet}
          timestamp="2 years ago"
        />
      </div>
    </div>
  );
}

export default RecommendedVideos;
